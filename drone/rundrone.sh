#!/usr/bin/env bash

set -ex

DRONE_GITHUB_CLIENT_ID="paste.your.github.OAuth.ID"
DRONE_GITHUB_CLIENT_SECRET="paste.your.github.OAuth.ID.secret"
DRONE_RPC_SECRET="paste.your.openssl.value"
DRONE_SERVER_HOST="127.0.0.1"
DRONE_SERVER_PROTO="https"

docker run \
  --volume=/var/lib/drone:/data \
  --env=DRONE_GITHUB_SERVER=https://github.com \
  --env=DRONE_GITHUB_CLIENT_ID=${DRONE_GITHUB_CLIENT_ID} \
  --env=DRONE_GITHUB_CLIENT_SECRET=${DRONE_GITHUB_CLIENT_SECRET} \
  --env=DRONE_AGENTS_ENABLED=true \
  --env=DRONE_RPC_SECRET=${DRONE_RPC_SECRET} \
  --env=DRONE_SERVER_HOST=${DRONE_SERVER_HOST} \
  --env=DRONE_SERVER_PROTO=${DRONE_SERVER_PROTO} \
  --env=DRONE_TLS_AUTOCERT=true \
  --publish=80:80 \
  --publish=443:443 \
  --restart=always \
  --detach=true \
  --name=drone \
  --env=DRONE_USER_CREATE=username:Wa-wann,admin:true \
  drone/drone:1

DRONE_RPC_HOST="127.0.0.1"
DRONE_RPC_PROTO="https"

docker run -d \
  -v /var/run/docker.sock:/var/run/docker.sock \
  -e DRONE_RPC_PROTO=${DRONE_RPC_PROTO} \
  -e DRONE_RPC_HOST=${DRONE_RPC_HOST} \
  --env=DRONE_RPC_SECRET=${DRONE_RPC_SECRET} \
  --env=DRONE_RUNNER_CAPACITY=2 \
  --env=DRONE_RUNNER_NAME=${HOSTNAME} \
  -p 3000:3000 \
  --restart=always \
  --name runner \
  drone/agent:1



